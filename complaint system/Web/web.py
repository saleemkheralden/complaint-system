from __future__ import absolute_import, division, print_function, unicode_literals
from flask import Flask, request, render_template, send_file, redirect, session, g, url_for, make_response
import os
from sql.sql_main import UPDATE, SELECT, DELETE, INSERT, connect
import subprocess
import os
import pyodbc

app = Flask(__name__)
# (id, name, link, offensive_count, offensive_toward, notes)

app.secret_key = os.urandom(24)
ip = subprocess.check_output("ipconfig").__str__().split("Wireless LAN adapter Wi-Fi")[1].__str__().split("IPv4 Address")[1].__str__().split(":")[1].__str__().split("\\r")[0].__str__().strip()

port = 5000


@app.route('/users')
def users():
    sort = request.args.get("sort")

    if sort == None:
        qry = "SELECT * FROM users"
    elif sort == "offensive":
        qry = "SELECT * FROM users ORDER BY offensive_count DESC"
    elif sort == "racist":
        qry = "SELECT * FROM users ORDER BY racist_count DESC"
    else:
        qry = "SELECT * FROM users"

    array = list()
    ds = SELECT(qry)
    for count, row in enumerate(ds, 1):
        ImgPath = row[len(row) - 1]
        temp = [count, ImgPath]
        for i, col in enumerate(row, 0):
            if i > 0:
                if i == 1:
                    temp.append('<a href="/posts?user=' + col + '">' + col + '</a>')
                elif i != len(row) - 1:
                    temp.append(col)
                else:
                    pass
        array.append(temp)
    return render_template('users.html', array=array)


@app.route('/', methods=['POST', 'GET'])
def index():
    updateMsg = ''
    if not 'data' in session:
        session['data'] = {'updateMsg': '', 'done': False}
    if session['data']['done']:
        updateMsg = session['data']['updateMsg']
        session['data']['done'] = False
        session['data']['updateMsg'] = ''
        session.modified = True

    array = list()
    qry = 'SELECT * FROM racist'
    ds = SELECT(qry)
    for row in ds:
        array.append(row[1])

    if request.method == 'POST':
        thisPath = os.path.dirname(__file__)
        ImagesPath = 'images\\'
        InsertImgPath = ''
        try:
            file = request.files.get('profileImg')
            name = file.filename
            data = file
            data.save(thisPath + '\\static\\' + ImagesPath + name)
            InsertImgPath = ImagesPath.replace('\\', '/') + name
        except:
            InsertImgPath = ImagesPath.replace('\\', '/') + 'avatar.png'
            pass

        username = request.form['name'].strip().replace("'", '')
        link = request.form['link'].replace("'", '')

        offen = request.form.get('offensiveCheckbox') != None

        race = request.form.get('racistCheckbox') != None

        toward = request.form['other'].replace("'", '')
        if request.form['other'] == '':
            try:
                toward = request.form['race']
            except:
                pass
        else:
            insertNewToward = "INSERT INTO racist (race) VALUES ('{}')".format(toward)
            INSERT(insertNewToward, 'racist')
            qry = 'SELECT * FROM racist'
            ds = SELECT(qry)
            for row in ds:
                array.append(row[1])

        content = request.form['content']
        content = content.replace("'", "")

        session['report'] = {"username": username,
                             "link": link,
                             "racist": 1 if race else 0,
                             "offensive":  1 if offen else 0,
                             "toward": toward,
                             "content": content}


        flag = True
        ds = SELECT("SELECT * FROM users WHERE name='{}'".format(session["report"]["username"]))
        for row in ds:
            session['user'] = {"id": row[0],
                               "username": row[1],
                               "racist": row[2],
                               "offensive":  row[3]}
            flag = False
            break

        if flag:
            insertUserQry = "INSERT INTO users (name, racist_count, offensive_count, img) VALUES ('{}', {}, {}, '{}')".format(
                session["report"]["username"], session["report"]["racist"], session["report"]["offensive"], InsertImgPath)
            INSERT(insertUserQry, 'users')
            ds = SELECT("SELECT * FROM users WHERE name='{}'".format(session["report"]["username"]))
            for row in ds:
                session['user'] = {"id": row[0],
                                   "username": row[1],
                                   "racist": row[2],
                                   "offensive": row[3]}
                break
        else:
            updateUserQry = "UPDATE users SET racist_count={}, offensive_count={}, img='{}' WHERE name='{}'".format(
                int(session['user']['racist']) + int(1 if race else 0),
                int(session['user']['offensive']) + int(1 if offen else 0), session['user']['username'], InsertImgPath)
            UPDATE(updateUserQry, 'users')
        insertUserPostQry = "INSERT INTO users_posts (id, toward, post_link, post_content) VALUES ({}, '{}', '{}', '{}')".format(
            session['user']['id'], session['report']['toward'], session['report']['link'], session['report']['content'])
        INSERT(insertUserPostQry, 'users_posts')
        updateMsg = """     <div class="alert alert-success alert-dismissible" style="margin-bottom:5px">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Updated <strong>successfully!</strong>
        </div>"""
        session['data'] = {'updateMsg': updateMsg, 'done': True}
        session.modified = True
        return redirect('/')

    return render_template('index.html', array=array, updateMsg=updateMsg)


@app.route('/posts')
def posts():
    requestedName = request.args.get("user")
    selectUserQry = "SELECT * FROM users WHERE name='" + requestedName + "'"
    ds = SELECT(selectUserQry)
    userId = None
    ImgPath = ''
    for row in ds:
        ImgPath = row[len(row) - 1]
        userId = row[0]
        break

    try:
        selectPostsQry = 'SELECT * FROM users_posts WHERE id={}'.format(userId)
        ds = SELECT(selectPostsQry)
        postsArray = list()
        for i, row in enumerate(ds, 1):
            link = "<a href='" + row[2] + "'>Post</a>"
            postsArray.append([i, row[1], link, row[3]])
    except:
        return redirect('/', code=302)

    return render_template('users_posts.html', postsArray=postsArray, ImgPath=ImgPath, name=requestedName)


@app.route('/image/default.jpg')
def load_logo():
    return send_file('static\\images\\avatar.png', mimetype='image/png')

if __name__ == "__main__":
    app.run(ip, port=port, debug=True)






