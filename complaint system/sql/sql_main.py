import pyodbc


def UPDATE(qry, tbl):
    innerFunc(qry, tbl)


def DELETE(qry, tbl):
    innerFunc(qry, tbl)


def INSERT(qry, tbl):
    try:
        innerFunc(qry, tbl)
        return True
    except Exception as e:
        print(e)
        return False


def SELECT(qry):
    conn = connect()

    cursor = conn.cursor()
    cursor.execute(qry)
    return cursor


def connect():
    return pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};Server=SLIMSPC;Database=complaint_DB;Trusted_Connection=yes;')


def innerFunc(qry, tbl):
    conn = connect()

    cursor = conn.cursor()
    cursor.execute('SELECT * FROM ' + tbl)

    cursor.execute(qry)
    conn.commit()